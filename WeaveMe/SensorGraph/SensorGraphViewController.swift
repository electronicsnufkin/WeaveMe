//
//  SensorGraphViewController.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 4/20/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//

import UIKit

class SensorGraphViewController: UIViewController {

    @IBOutlet weak var sensorDistributionView: BarGraphView!
    @IBOutlet weak var valueDeviationBarGraphView: ValueDeviationBarGraphView!
    
    lazy var histogramDriver = ADCHistogramGraphDriver(barGraphView: sensorDistributionView, command: "gr", annotation: "Get Right IR Sensor Raw Value")

    let camWidthScanner = CamWidthScanner()

    override func viewDidLoad() {
        super.viewDidLoad()

        // This is just some hard coded data from the first scan of
        // rising edges.
        valueDeviationBarGraphView.setValuesFromPositions([409, 1086, 1687, 2300, 2921, 3521, 4152, 4737, 5342, 5965, 6598, 7191, 7799, 8396, 9008, 9597, 10201, 10793, 11389, 11986, 12598, 13195, 13799, 14414, 15013, 15612, 16194, 16825, 17424, 18030, 18636])

        // TODO: Remove this and actually display the info some place.
        Loom.shared.getWarpState(forceUpdate: true) { (warpState, error) in
            if let warpState = warpState {
                NSLog("Got layer states \(warpState.layerStates)")
            } else {
                NSLog("Error getting layer states \(error?.localizedDescription ?? "nil")")
            }
        }
    }

    @IBAction func close(_ sender: Any) {
        dismiss(animated: true) {
            NSLog("Dismissed")
        }
    }

    // This was only useful to test how much noise we were getting with motors
    // powered but not moving.
    @IBAction func scanDistribution(_ sender: Any) {
//        NSLog("Sending IR sensor distribution")
//        camWidthScanner.scan { (risingEdges, fallingEdges) in
//            NSLog("Done")
//        }
        histogramDriver.populateWithFakeDataAndDisplay()
        histogramDriver.update(with: 256) {
            NSLog("Got sensor distribution" )
        }
    }

    class func instantiate() -> SensorGraphViewController {
        let storyboard = UIStoryboard(name: "SensorGraphViewController", bundle: nil)
        guard let sensorGraphViewController = storyboard.instantiateViewController(withIdentifier: "SensorGraphViewController") as? SensorGraphViewController else {
            fatalError()
        }
        return sensorGraphViewController
    }
}
