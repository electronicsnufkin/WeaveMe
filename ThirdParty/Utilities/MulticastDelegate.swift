//
//  MulticastDelegate.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/10/18.
//
//  From http://www.gregread.com/2016/02/23/multicast-delegates-in-swift/
//
//  This provides a weak wrapper around class objects that implement a specific protocol.

import UIKit

class MulticastDelegate <T> {
    private var weakDelegates = [WeakWrapper]()
    
    func add(delegate: T) {
        // If delegate is a class, add it to our weak reference array
        weakDelegates.append(WeakWrapper(value: delegate as AnyObject))
    }
    
    func remove(delegate: T) {
        // If delegate is an object, let's loop through weakDelegates to
        // find it.
        
        for (index, delegateInArray) in weakDelegates.enumerated().reversed() {
            // If we have a match, remove the delegate from our array
            if delegateInArray.value === (delegate as AnyObject) {
                weakDelegates.remove(at: index)
            }
        }
    }
    
    func invoke(invocation: (T) -> ()) {
        // Enumerating in reverse order prevents a race condition from happening when removing elements.
        for (index, delegate) in weakDelegates.enumerated().reversed() {
            // Since these are weak references, "value" may be nil
            // at some point when ARC is 0 for the object.
            if let delegate = delegate.value {
                invocation(delegate as! T)
            }
                // Else, ARC killed it, get rid of the element from our
                // array
            else {
                weakDelegates.remove(at: index)
            }
        }
    }
}

// I've removed the += and -= operators.  -Kurt

private class WeakWrapper {
    weak var value: AnyObject?
    
    init(value: AnyObject) {
        self.value = value
    }
}

