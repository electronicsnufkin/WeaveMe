//
//  WeavingImageSource.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/30/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  When rendering an image we need various different shaped/colored
//  image pieces that are plunked down during rendering.  A source like this generates and
//  holds onto specific color/size versions of each the thread types.  These types are determined
//  by the weaving pattern, and re-use should be very high.
//
//  In general use set the dimension in pixels, and then begin requesting images.  The first
//  few images will be very expensive to get, but as types/colors repeat they get much faster.
//

import UIKit

class WeavingImageSource {

    /// This creates an image based on the tintColor/nativeImageSize and our standard images. This bakes the color
    /// into the image, but also maintains the alpha so we can in other contexts just draw the image and be done.
    /// Later if we have fancier images we may do something else.
    func tintedImageForType(_ threadImageType: ThreadImageType, tintColor: UIColor) -> UIImage? {
        let imageKey = imageCacheKey(threadImageType: threadImageType, color: tintColor)
        if let cachedImage = tintedImageCache[imageKey] {
            return cachedImage
        }

        guard let sourceImage = sourceImageForType(threadImageType) else {
            return nil
        }

        UIGraphicsBeginImageContextWithOptions(nativeImageSize, false, 0)

        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return nil
        }

        // We still center the image even if it's not the native height/width.
        var fullRect = CGRect.init()
        let sourceWidth = max(sourceImage.size.width, 1.0)
        let sourceHeight = max(sourceImage.size.height, 1.0)
        let aspect = sourceWidth/sourceHeight

        // If it's wider than it is tall, we center it vertically and have width go full width
        if sourceWidth >= sourceHeight {
            fullRect.size.width = nativeImageSize.width
            fullRect.size.height = nativeImageSize.width/aspect
            fullRect.origin.x = 0
            fullRect.origin.y = (nativeImageSize.height - fullRect.size.height)*0.5
        } else {
            fullRect.size.height = nativeImageSize.height
            fullRect.size.width = aspect*nativeImageSize.height
            fullRect.origin.x = (nativeImageSize.width - fullRect.size.width)*0.5
            fullRect.origin.y = 0
        }

        tintColor.setFill()
        context.fill(fullRect)

        sourceImage.draw(in: fullRect, blendMode: .overlay, alpha: 1.0)
        sourceImage.draw(in: fullRect, blendMode: .destinationIn, alpha: 1.0)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        tintedImageCache[imageKey] = newImage

        return newImage
    }

    /// The nxn size of the images held by the cache. Changing this invalidates the cache so
    /// it should mostly be set and forget.
    var nativeImageSize = CGSize.init(width: 60.0, height: 60.0) {
        didSet {
            if nativeImageSize != oldValue {
                tintedImageCache.removeAll()
            }
        }
    }

    private var sourceImageCache: [String: UIImage] = [:]
    private var tintedImageCache: [Int: UIImage] = [:]

    // Build a key for this image type/color.
    // This is actually called a ton, and is kind of slow with all this wacky string building.
    private func imageCacheKey(threadImageType: ThreadImageType, color: UIColor) -> Int {
        return threadImageType.hashValue ^ color.hashValue &* 16777619
    }

    private func sourceImageForType(_ threadImageType: ThreadImageType) -> UIImage? {
        if let cachedImage = sourceImageCache[threadImageType.imageName] {
            return cachedImage
        }

        guard let image = UIImage(named: threadImageType.imageName) else {
            return nil
        }
        // TODO: For colorful images it would be much better to sample these down
        // to nativeImageSize first before caching.
        sourceImageCache[threadImageType.imageName] = image
        return image
    }

}
